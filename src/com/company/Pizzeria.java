package com.company;

import java.util.ArrayList;

/**
 * Клас, що створює масив піц
 */
public class Pizzeria {

    public static void main(String[] args) {
/**
 * створюємо 4 види піци
 */
        Pizza pizza1 = new Pizza("mozzarella", "Margarita", 250, 36.5);
        Pizza pizza2 = new Pizza("ham & salami", "Meat", 210, 25.0);
        Pizza pizza3 = new Pizza("mushrooms & tomatoes", "Capricious", 190, 10.0);
        Pizza pizza4 = new Pizza("chicken & pineapple", "Hawaiian", 280, 45.0);

/**
 * створюємо масив з 4 видів піци
 */
        ArrayList<Pizza> pizzas = new ArrayList<>();
        pizzas.add(pizza1);
        pizzas.add(pizza2);
        pizzas.add(pizza3);
        pizzas.add(pizza4);

        /**
         * цикл для перебору масиву піц та виведенню інформації про кожну з піц
         */
        int i = 1;

        for (Pizza p : pizzas) {
            String composition = p.composition;
            String name = p.name;
            double cost = p.cost;
            double diameter = p.basic.getRadius() * 2;

            System.out.println("Pizza " + i + ": " + composition + ", " + name + ", " + cost + ", " + diameter);

            i++;

        }
    }
}
